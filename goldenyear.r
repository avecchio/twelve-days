input<-file('stdin', 'r')
row <- readLines(input, n=1)
date <- as.numeric(unlist(strsplit(row, '-')))
goldenYear <- date[2] + date[3]
cat(gsub(date[3], goldenYear, row))
